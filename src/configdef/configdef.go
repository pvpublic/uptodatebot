package configdef

import (
	"io/ioutil"
	"log"
	"gopkg.in/yaml.v2"
)

type Config struct {
	Server   string `yaml:"Server"`
	Password string `yaml:"Password"`
	Port     int    `yaml:"Port"`
	Channel  string `yaml:"Channel"`
	BotNick  string `yaml:"BotNick"`
	CacheDir string `yaml:"CacheDir"`
}

func (c *Config) ReadFromYAML(fp string) {
	configBytes, err := ioutil.ReadFile(fp)
	if err != nil {
		log.Panic(err)
	}
	yaml.Unmarshal(configBytes, c)
}
