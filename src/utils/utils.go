package utils

import (
	"os"
	"log"
)

func In(sl []string, el string) bool {
	for _, e := range sl {
		if e == el {
			return true
		}
	}
	return false
}

func Keys(dict map[string]bool) []string {
	k := make([]string, 0)
	for key, _ := range dict {
		k = append(k, key)
	}
	return k
}

func FileExists(path string) bool {
	_, err := os.Stat(path)
	if err == nil { return true } 
	if os.IsNotExist(err) { return false } else { log.Fatalln(err) }
	return false
}
