# UpToDateBot

An IRC bot to keep you up to date.

UpToDateBot lingers on an IRC channel, and registers new users as they join. When they leave, it logs the messages they miss, and privately messages them to the concerned user when they rejoin.

## Configuration

You need to place a `config.yaml` config file in UpToDateBot's running directory.
It must have the following content.

```
Server: irc.example.com
Password: super_secret_password
Port: 6667
BotNick: MyUpToDateBot
Channel: "#op"
CacheDir: /place/to/cache
```

## Docker

The dockerfile included can be used to build a container with UpToDateBot in it.
In the repository's root directory, build with:

```
docker build -t uptodatebot .
```

And to run:

```
docker run -v ${PWD}/config.yaml:/etc/uptodatebot/config.yaml uptodatebot
```

The config file must be `/etc/uptodatebot/config.yaml` in the docker container.

