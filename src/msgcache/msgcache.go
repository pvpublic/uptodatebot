package msgcache

import (
	"bitbucket.org/pvpublic/uptodatebot/src/utils"
	"os"
	"io/ioutil"
	"strings"
	"path/filepath"
)

type MsgCache struct {
	CacheDir string
}

//New creates a new MsgCache with the given directory as its location
func New(cacheDir string) MsgCache {
	if !utils.FileExists(cacheDir) {
		os.MkdirAll(cacheDir, 0755)
	}
	mc := MsgCache{CacheDir: cacheDir}
	return mc
}

//cacheFile returns the path to the cache file for the given user
func (m MsgCache) cacheFile(user string) string {
	return filepath.Join(m.CacheDir, user)
}

//HasFor returns whether the given user has cached messages
func (m MsgCache) HasFor(user string) bool {
	p := m.cacheFile(user)
	if utils.FileExists(p) {
		bytes, _ := ioutil.ReadFile(p)
		return len(bytes) > 0
	}
	return false
}

//For returns a string slice of cached messages
func (m MsgCache) For(user string) []string {
	p := m.cacheFile(user)
	if utils.FileExists(p) {
		bytes, _ := ioutil.ReadFile(p)
		return strings.Split(string(bytes), "\n")
	}
	return make([]string, 0)
}

//StartAfresh empties the cache for the given user, creating one if necessary
func (m MsgCache) StartAfresh(user string) {
	f, _ := os.Create(m.cacheFile(user))
	f.Close()
}

//AddFor appends the given message to the cache for the given user
func (m MsgCache) AddFor(msg, user string) {
	f, _ := os.OpenFile(m.cacheFile(user), os.O_APPEND|os.O_WRONLY, 0755)
	f.WriteString(msg + "\n")
	f.Close()
}

//Users returns a slice of usernames whose caches exist
func (m MsgCache) Users() []string {
	files, _ := ioutil.ReadDir(m.CacheDir)
	usrs := make([]string, 0)
	for _, file := range files {
		usrs = append(usrs, file.Name())
	}
	return usrs
}
