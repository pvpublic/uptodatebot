FROM golang:latest as buildcontainer

WORKDIR /build
RUN git clone https://PrithviVishak@bitbucket.org/pvpublic/uptodatebot.git
WORKDIR /build/uptodatebot
RUN go build -o UpToDateBot

FROM alpine:latest as uptodatebot
LABEL maintainer="Prithvi Vishak <prithvivishak@gmail.com>"

COPY --from=buildcontainer /build/uptodatebot/UpToDateBot /
RUN apk add --no-cache gcompat

WORKDIR /etc/uptodatebot
CMD [ "/UpToDateBot" ]
