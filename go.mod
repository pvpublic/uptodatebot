module bitbucket.org/pvpublic/uptodatebot

go 1.17

require (
	github.com/belak/irc v2.1.0+incompatible
	gopkg.in/yaml.v2 v2.4.0
)

require github.com/stretchr/testify v1.7.0 // indirect
