package main

import (
	"github.com/belak/irc"
	"net"
	"log"
	"strings"
	"strconv"
	"time"
	"bitbucket.org/pvpublic/uptodatebot/src/configdef"
	"bitbucket.org/pvpublic/uptodatebot/src/msgcache"
)

var config configdef.Config
var msgCache msgcache.MsgCache
var users = make(map[string]bool, 0)
var joined bool = false

func handleMessage(c *irc.Client, m *irc.Message) {
	log.Println(m)
	if joined && m.Prefix.Name != config.BotNick {
		switch strings.ToLower(m.Command) {
		case "join":
			time.Sleep(time.Second * 2)
			if msgCache.HasFor(m.Prefix.Name) {
				c.Write("PRIVMSG " + m.Prefix.Name + " :Hey " + m.Prefix.Name + "! You missed these messages:")
				for _, line := range msgCache.For(m.Prefix.Name) {
					c.Write("PRIVMSG " + m.Prefix.Name + " :" + line)
					time.Sleep(time.Millisecond * 100)
				}
			}
			users[m.Prefix.Name] = true

		case "quit":
			msgCache.StartAfresh(m.Prefix.Name)
			users[m.Prefix.Name] = false

		case "privmsg":
			for username, _ := range users {
				if users[username] == false {
					msgCache.AddFor(m.Prefix.Name + ": " + m.Trailing(), username)
				}
			}

		case "353": // NAMES (nicks of those who are online)
			for _, username := range strings.Split(m.Trailing(), " ") {
				username = strings.ReplaceAll(username, "@", "")
				username = strings.ReplaceAll(username, "+", "")
				if username != config.BotNick {
					users[username] = true
				}
			}
		}

	} else if m.Command == "001" {
		// 001 is a welcome event, so we join channels there
		c.Write("JOIN " + config.Channel)
		c.Write("PRIVMSG #op :Hey everyone! I'm a bot that'll keep you up to date on what you missed.")
		joined = true
		for _, usr := range msgCache.Users() {
			users[usr] = false // Users who are online are updated according to the NAMES (353) reported by the server
		}
	}
}

func main() {
	config.ReadFromYAML("config.yaml")

	msgCache = msgcache.New(config.CacheDir)

	conn, err := net.Dial("tcp", config.Server + ":" + strconv.Itoa(config.Port))
	if err != nil {
		log.Fatalln(err)
	}

	ircConfig := irc.ClientConfig{
		Nick: config.BotNick,
		Pass: config.Password,
		User: config.BotNick,
		Name: config.BotNick,
		Handler: irc.HandlerFunc(handleMessage),
	}

	client := irc.NewClient(conn, ircConfig)
	err = client.Run()
	if err != nil {
		log.Fatalln(err)
	}
}
